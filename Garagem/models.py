from django.db import models
from Pessoa.models import Usuario

class Veiculo(models.Model):
    veiculoId = models.AutoField(primary_key=True)
    cor = models.CharField(max_length = 20)
    anoFabricacao = models.IntegerField()
    modelo = models.CharField(max_length = 60)
    moto = models.BooleanField(default = False)
    dataInsercao = models.DateTimeField(auto_now_add = True, null=True)
    dataAtualizacao = models.DateTimeField(auto_now = True)

class Garagem(models.Model):
    GaragemId = models.AutoField(primary_key=True)
    Livre = models.BooleanField(default = True)
    Ativa = models.BooleanField(default = True)
    Usuario = models.ForeignKey(Usuario, to_field="Telefone", db_column="Usuario", on_delete=models.CASCADE, null=True)
    VeiculoId = models.ForeignKey(Veiculo, on_delete=models.CASCADE, null=True)
    Identificador = models.CharField(max_length=50, null=True)
    DataInsercao = models.DateTimeField(auto_now_add = True, null=True)
    DataAtualizacao = models.DateTimeField(auto_now = True)

class EntradaSaida(models.Model):
    EntradaSaidaId = models.AutoField(primary_key=True)
    VeiculoId = models.ForeignKey(Veiculo, on_delete=models.CASCADE)
    GaragemId = models.ForeignKey(Garagem, on_delete=models.CASCADE)
    Operacao = models.CharField(max_length = 7)
    Horario = models.DateTimeField(auto_now = True)
    DataInsercao = models.DateTimeField(auto_now_add = True)
    DataAtualizacao = models.DateTimeField(auto_now = True)