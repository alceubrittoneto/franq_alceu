from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from Garagem.models import Garagem, Veiculo, EntradaSaida
from Pessoa.models import Usuario
from Pessoa.serializers import PessoaSerializer
from Garagem.serializers import VeiculoSerializer, GaragemSerializer, EntradaSaidaSerializer
import json

@csrf_exempt
def number2Garagem(request,id=0):
    if request.method == 'GET':
        garagem = Garagem.objects.all()
        garagem_serializer = GaragemSerializer(garagem, many=True)
        return JsonResponse(garagem_serializer.data, safe=False)
    elif request.method == 'POST':
        garagem_data = JSONParser().parse(request)
        if garagem_data["Identificador"]:
            garagem_serializer = GaragemSerializer(data = garagem_data)
            if garagem_serializer.is_valid():
                garagem_serializer.save()
                return JsonResponse("Garagem criada com sucesso!", safe=False)
        else:
            return JsonResponse("Campo 'Identificador' requerido", safe=False)
    elif request.method == 'PUT': 
        garagem_data = JSONParser().parse(request)
        garagem = Garagem.objects.get(GaragemId=garagem_data['GaragemId'])
        garagem_serializer = GaragemSerializer(garagem, data = garagem_data) 
        if garagem_serializer.is_valid():
            garagem_serializer.save()
            return JsonResponse("Atualização efetuada com sucesso!", safe=False)
        return JsonResponse("Erro ao atualizar a garagem.")

@csrf_exempt
def number2Veiculo(request,id=0):
    if request.method == 'GET':
        veiculo = Veiculo.objects.all()
        veiculo_serializer = VeiculoSerializer(veiculo, many=True)
        return JsonResponse(veiculo_serializer.data, safe=False)
    elif request.method == 'POST':
        veiculo_data = JSONParser().parse(request)
        veiculo_serializer = VeiculoSerializer(data = veiculo_data)
        if veiculo_serializer.is_valid():
            veiculo_serializer.save()
            return JsonResponse("Veículo adicionado com sucesso!", safe=False)
        return JsonResponse("Ocorreu um erro ao adicionar o veículo.", safe=False)
    elif request.method == 'PUT': 
        veiculo_data = JSONParser().parse(request)
        veiculo = Veiculo.objects.get(VeiculoId=veiculo_data['VeiculoId'])
        veiculo_serializer = VeiculoSerializer(veiculo, data = veiculo_data)
        if veiculo_serializer.is_valid():
            veiculo_serializer.save()
            return JsonResponse("Atualização efetuada com sucesso!", safe=False)
        return JsonResponse("Erro ao atualizar o veículo.")

@csrf_exempt
def number2_entradaGaragem(request):
    if request.method == 'GET':
        entradasaida = EntradaSaida.objects.filter(Operacao = 'ENTRADA')
        entradasaida_serializer = EntradaSaidaSerializer(entradasaida, many=True)
        return JsonResponse(entradasaida_serializer.data, safe=False)
    elif request.method == 'POST':
        entradasaida_data = JSONParser().parse(request)
        entradasaida_serializer = EntradaSaidaSerializer(data = entradasaida_data)
        garagem = Garagem.objects.get(GaragemId = entradasaida_data['GaragemId'])
        if garagem.Livre == 'True' or garagem.Ativa == 'True':
            if entradasaida_serializer.is_valid():
                entradasaida_serializer.save()
                garagem = Garagem.objects.get(GaragemId = entradasaida_data['GaragemId'])
                garagem.Livre = 'False'
                garagem.save()
                return JsonResponse("Entrada efetuada com sucesso!", safe=False)
            return JsonResponse("Ocorreu um erro ao cadastrar a entrada do veículo.", safe=False)
        else:
            print("A garagem está ocupada.")       
    elif request.method == 'PUT': 
        entradasaida_data = JSONParser().parse(request)
        entradasaida = EntradaSaida.objects.get(EntradaSaidaId=entradasaida_data['EntradaSaidaId'])
        entradasaida_serializer = EntradaSaidaSerializer(entradasaida, data = entradasaida_data)
        if entradasaida_serializer.is_valid():
            entradasaida_serializer.save()
            return JsonResponse("Atualização efetuada com sucesso!", safe=False)
        return JsonResponse("Erro ao atualizar a operação.")

@csrf_exempt
def number2_saidaGaragem(request):
    if request.method == 'GET':
        entradasaida = EntradaSaida.objects.filter(Operacao = 'SAIDA')
        entradasaida_serializer = EntradaSaidaSerializer(entradasaida, many=True)
        return JsonResponse(entradasaida_serializer.data, safe=False)
    elif request.method == 'POST':
        entradasaida_data = JSONParser().parse(request)
        entradasaida = EntradaSaida.objects.filter(Operacao = 'ENTRADA', VeiculoId = entradasaida_data['VeiculoId'], GaragemId = entradasaida_data['GaragemId'])
        if entradasaida:
                entradasaida_serializer = EntradaSaidaSerializer(data = entradasaida_data)
                if entradasaida_serializer.is_valid():
                    entradasaida_serializer.save()
                    garagem = Garagem.objects.get(GaragemId = entradasaida_data['GaragemId'])
                    garagem.Livre = 'True'
                    garagem.save()
                    return JsonResponse("Saida efetuada com sucesso!", safe=False)
                return JsonResponse("Ocorreu um erro ao cadastrar a saida do veículo.", safe=False)
        else:
            print("O veículo que está tentando sair nem sequer entrou na garagem.")    
    elif request.method == 'PUT': 
        entradasaida_data = JSONParser().parse(request)
        entradasaida = EntradaSaida.objects.get(EntradaSaidaId=entradasaida_data['EntradaSaidaId'])
        entradasaida_serializer = EntradaSaidaSerializer(entradasaida, data = entradasaida_data)
        if entradasaida_serializer.is_valid():
            entradasaida_serializer.save()
            return JsonResponse("Atualização efetuada com sucesso!", safe=False)
        return JsonResponse("Erro ao atualizar a operação.")        


@csrf_exempt
def apiCriaGaragemUsuario(num_tel):
    garagem_data = {
        "Livre": "true",
        "Ativa": "true",
        "Usuario": num_tel
    }
    garagem_serializer = GaragemSerializer(data = garagem_data)
    if garagem_serializer.is_valid():
        garagem_serializer.save()
        return JsonResponse("Garagem criada com sucesso!", safe=False)
    return JsonResponse("Ocorreu um erro ao criar a garagem.", safe=False)


@csrf_exempt
def vinculaVeiculoGaragem(request, id=0):
    dados_req = JSONParser().parse(request)
    for d in dados_req["Veiculo"]:
        new_json ={
            "Usuario": dados_req["Usuario"],
            "Identificador": dados_req["Identificador"],
            "VeiculoId": d,
            "Livre": "false",
            "Ativo": "true"
        }
        garagem_serializer = GaragemSerializer(data = new_json)
        if garagem_serializer.is_valid():
            garagem_serializer.save()
    return JsonResponse("Sucesso.", safe=False)

@csrf_exempt
def getVeiculosVinculados(request, id=0):
    veic_vinc_data = JSONParser().parse(request)
    get_user = Garagem.objects.filter(Usuario = veic_vinc_data["Usuario"])
    veic_vinc = GaragemSerializer(get_user, many=True)
    return JsonResponse(veic_vinc.data, safe=False)

@csrf_exempt
def apiConsultasGerais(self):
    usuario = Usuario.objects.all()
    usuario_serializer = PessoaSerializer(usuario, many=True)
    garagem = Garagem.objects.all()
    garagens_ativas = Garagem.objects.filter(Ativa = 'True')
    garagens_ativas_serializer = GaragemSerializer(garagens_ativas, many=True)
    usuario_garagem = Garagem.objects.filter(Usuario__isnull=False).distinct('Identificador')
    usuario_garagem_serializer = GaragemSerializer(usuario_garagem, many=True)
    print(usuario_garagem)
    #usuario_gar = Usuario.objects.filter(UsuarioId = usuario_garagem)

    new_json = {
        "usuarios_cadastrados":
            usuario_serializer.data,
        "garagens_ativas": 
            garagens_ativas_serializer.data
    }
    return JsonResponse(new_json, safe=False)

@csrf_exempt
def getVeicMoto(request, id=0):
    veiculo = Veiculo.objects.filter()
    final_json = []
    for v in veiculo.values():
        if v["moto"] == False:
            new_json = {
                "cor": v["cor"],
                "anofabricacao": v["anoFabricacao"]
            }        
            final_json.append(new_json)
        else:
            new_json = {
                "modelo":v["modelo"],
                "anoFabricacao": v["anoFabricacao"]
            }
            final_json.append(new_json)
    return JsonResponse(final_json, safe=False)    

