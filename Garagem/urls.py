from django.conf.urls import url
from Garagem import views

urlpatterns = [
    url(r'^garagem$', views.number2Garagem),
    url(r'^garagem/([0-9]+)$', views.number2Garagem),
    url(r'^veiculo$', views.number2Veiculo),
    url(r'^veiculo/([0-9]+)$', views.number2Veiculo),
    url(r'^entradaGaragem', views.number2_entradaGaragem),
    url(r'^saidaGaragem', views.number2_saidaGaragem),
    url(r'^vinculaVeiculoGaragem', views.vinculaVeiculoGaragem),
    url(r'getVeiculosVinculados', views.getVeiculosVinculados),
    url(r'apiConsultasGerais', views.apiConsultasGerais),
    url(r'getVeicMoto', views.getVeicMoto),
]
