from rest_framework import serializers
from Garagem.models import Garagem, Veiculo, EntradaSaida

class GaragemSerializer(serializers.ModelSerializer):
    class Meta:
        model=Garagem
        fields=('GaragemId', 'Livre', 'Ativa', 'Usuario', 'VeiculoId', 'Identificador')

class VeiculoSerializer(serializers.ModelSerializer):
    class Meta:
        model=Veiculo
        fields=('veiculoId', 'cor', 'anoFabricacao', 'modelo', 'moto')

class EntradaSaidaSerializer(serializers.ModelSerializer):
    class Meta:
        model=EntradaSaida
        fields=('EntradaSaidaId', 'VeiculoId', 'GaragemId', 'Operacao', 'Horario')               