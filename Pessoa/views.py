from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from Pessoa.models import Usuario
from Pessoa.serializers import PessoaSerializer
from Garagem.views import apiCriaGaragemUsuario

@csrf_exempt

def number1(request, id=0):
    if request.method == 'GET':
        usuario = Usuario.objects.all()
        usuario_serializer = PessoaSerializer(usuario, many=True)
        return JsonResponse(usuario_serializer.data, safe=False)
    elif request.method == 'POST':
        usuario_data = JSONParser().parse(request)
        usuario_serializer = PessoaSerializer(data = usuario_data)
        param = 0
        if usuario_serializer.is_valid():
            usuario_serializer.save()
            if param == 1:
                ult = Usuario.objects.get(Telefone=usuario_data['Telefone'])
                apiCriaGaragemUsuario(ult.Telefone)
            return JsonResponse("Usuário criado com sucesso!", safe=False)
        return JsonResponse("Ocorreu um erro ao adicionar o usuário.", safe=False)
    elif request.method == 'PUT': 
        usuario_data = JSONParser().parse(request)
        usuario = Usuario.objects.get(UsuarioId=usuario_data['UsuarioId'])
        usuario_serializer = PessoaSerializer(usuario, data = usuario_data)
        if usuario_data['Ativo'] == False and usuario_serializer.is_valid():
            usuario_serializer.save()
            return JsonResponse("Usuário excluído com sucesso!", safe=False)
        if usuario_serializer.is_valid():
            usuario_serializer.save()
            return JsonResponse("Atualização efetuada com sucesso!", safe=False)
        return JsonResponse("Erro ao atualizar o usuário.")
