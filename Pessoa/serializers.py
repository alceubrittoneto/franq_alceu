from rest_framework import serializers
from Pessoa.models import Usuario

class PessoaSerializer(serializers.ModelSerializer):
    class Meta:
        model=Usuario
        fields=('UsuarioId', 'Nome', 'Email', 'Telefone', 'DataInsercao', 'DataAtualizacao', 'Ativo', 'Tipo')