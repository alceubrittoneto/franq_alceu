from django.db import models
from django.core.validators import RegexValidator

# Create your models here.

class Usuario(models.Model):
    UsuarioId = models.AutoField(primary_key = True)
    Nome = models.CharField(max_length = 100)
    Email = models.CharField(max_length = 100)
    TelefoneRegex = RegexValidator(regex = r"^\+?1?\d{8,15}$")
    Telefone = models.CharField(validators = [TelefoneRegex], max_length = 12, unique = True)
    Ativo = models.BooleanField(default = True)
    Tipo = models.IntegerField(default = 1) #1 - Consumidor 2 - Administrador
    DataInsercao = models.DateTimeField(auto_now_add = True)
    DataAtualizacao = models.DateTimeField(auto_now = True)

